<?php

use Faker\Generator as Faker;
 
$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->name, 
        'user_id' => function() {
            return factory('App\Models\User')->create()->id;
        },
    ];
});
