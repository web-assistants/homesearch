<?php

namespace Tests\Feature\Post;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReadPostTest extends TestCase
{
    use RefreshDatabase; 

    public function setUp() {
        parent::setUp(); 
    }

    /**
     * Fetch no existent post
     * 
     * @test
     */
    public function fetchNonExistentPost() {
        $response = $this->json('GET', '/api/post/99999999');

        $response->assertStatus(404);
    }

    /**
     * Fetch existing post
     * 
     * @test
     */
    public function fetchExistingPost() {
        $data = $this->generateData();

        $response = $this->json('GET', '/api/post/' . $data->post_id);
       
        $response->assertStatus(200)
                 ->assertJsonStructure([
                    'id',
                    'title',
                    'username', 
                    'comments'
                 ]); 
    }

    /**
     * Method generates dummy data
     */
    private function generateData() { 
        return factory(\App\Models\Comment::class)->create();
    }
}
