<?php

namespace Tests\Feature\User;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Mail;
use App\Mail\User\UserDeleted;

class DeleteUserTest extends TestCase
{
    use RefreshDatabase; 
    
    public function setUp() {
        parent::setUp(); 
    }

    /**
     * Delete non-existent user
     *
     * @test
     */
    public function deleteNonExistentUser()
    {
        $response = $this->json('DELETE', '/api/user/9999999');

        $response->assertStatus(404);
    }

    /**
     * Delete user
     * 
     * @test
     */
    public function deleteUser() { 
        $data = $this->generateData(); 
        Mail::fake();

        // call end point
        $response = $this->json('DELETE', '/api/user/' . $data->user_id);

        // verify response
        $response->assertStatus(200)
                 ->assertJson([
                     'type' => 'success',
                     'message' => 'User was deleted'
                 ]);

        // verify database
        $this->assertDatabaseMissing('users', ['id' => $data->user_id]); // user was deleted
        $this->assertDatabaseMissing('posts', ['user_id' => $data->user_id]); // posts were deleted
        $this->assertDatabaseMissing('comments', ['user_id' => $data->user_id]); // comments were deleted 

        // verify that email was sent
        Mail::assertSent(UserDeleted::class, 1);
    }

    /**
     * Method generates dummy data
     */
    private function generateData() { 
        return factory(\App\Models\Comment::class)->create();
    }
}
