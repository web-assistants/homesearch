<?php
namespace App\Services;

use App\Models\User;  
use App\Events\User\UserDeleted;

/**
 * Class handles CRUD related tasks for User feature
 */
class UserService 
{ 

    /**
     * Method deletes user and all related item from the database
     * 
     * @param User $user 
     */
    public function delete(User $user) 
    {  
        // delete user
        if($user->delete()) { 
            // send email
            event(new UserDeleted($user));

            return true;
        }

        return false;
    }
}