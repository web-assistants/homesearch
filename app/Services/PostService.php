<?php
namespace App\Services;

use App\Models\Post; 
use App\Repositories\PostRepo;

/**
 * Class handles CRUD related tasks for Posts feature
 */
class PostService 
{
    protected $postRepo;

    public function __construct() 
    {
        $this->postRepo = new PostRepo();
    }

    /**
     * Get a post
     * 
     * @param Post $post
     * @param boolean $comments used to switch comments on/off
     * @return Illuminate\Database\Query\Builder
     */
    public function getPost(Post $post, $comments = false) 
    {
        $return = $this->postRepo
            ->getPost($post->id)
            ->first();

        // display comments?
        if($comments == true) {
            $return->comments = $this->postRepo
                ->getComments($post->id)
                ->get();
        } 

        return collect($return);
    } 
}