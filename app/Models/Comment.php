<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $timestamps = false;
 
    /**
     * Comment belongs to a single user
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() 
    {
        return $this->belongsTo("App\Models\User");
    }

    /**
     * Comment belongs to a single post
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post() 
    {
        return $this->belongsTo("App\Models\Post");
    }
}
