<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable; 

    public $timestamps = false;
 
    /**
     * User may have multiple posts
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts() 
    {
        return $this->hasMany("App\Models\Post");
    }
}
