<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false;

    /**
     * Post may have multiple comments
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments() 
    {
        return $this->hasMany("App\Models\Comment");
    }

    /**
     * Post belongs to a single user
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() 
    {
        return $this->belongsTo("App\Models\User");
    }
}
