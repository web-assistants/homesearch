<?php

namespace App\Listeners\User;

use App\Events\User\UserDeleted;
use App\Mail\User\UserDeleted as UserDeletedMail; 
use Mail;

class SendUserDeletedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserDeleted $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
        // get user object
        $user = $event->user; 
        
        // send mail
        Mail::to($user)->send(new UserDeletedMail($user));
    }
}
