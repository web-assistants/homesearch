<?php

namespace App\Http\Controllers;
 
use App\Models\Post;
use App\Services\PostService;

class PostController extends Controller
{
    protected $postServ;

    public function __construct() 
    {
        $this->postServ = new PostService();
    }

    /**
     * method handles display of the post
     * 
     * @param Post $post
     * @return json
     */
    public function show(Post $post) 
    { 
        $postArr = $this->postServ
            ->getPost($post, true)
            ->toArray();

        return response()->json($postArr); 
    }
}
