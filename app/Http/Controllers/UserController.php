<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Models\User;

class UserController extends Controller 
{

    protected $userServ;

    public function __construct() 
    {
        $this->userServ = new UserService();
    }

    /**
     * Method deletes user from the system
     * 
     * @param User $user 
     * @return json
     */
    public function destroy(User $user) 
    {
        if($this->userServ->delete($user)) {  
            return response()->json([
                'type' => 'success',
                'message' => 'User was deleted',
            ]);
        }

        // possible mysql error
        return response()->json([
            'type' => 'error',
            'message' => 'User could not be deleted',
        ], 500);
    }
}
