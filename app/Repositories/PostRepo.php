<?php
namespace App\Repositories;

use DB;

/**
 * Repository file for the post
 */
class PostRepo {
    /**
     * Get the post
     * 
     * @param int $id id of the post
     * @return Illuminate\Database\Query\Builder
     */
    public function getPost($id) 
    {
        return DB::table("posts")->select([
            'posts.id',
            'posts.title',
            DB::raw('users.name as username'),
            //'content' // an empty field???
        ])
        ->join('users', 'posts.user_id', '=', 'users.id')
        ->where('posts.id', $id);
    }

    /** 
     * Get comments for the post in question
     * 
     * @param int $id id of the post
     * @return Illuminate\Database\Query\Builder
     */
    public function getComments($id) 
    {
        return DB::table('comments')->select([
            'post_id',
            'text'
        ])
        ->where('comments.post_id', $id);
    }
}