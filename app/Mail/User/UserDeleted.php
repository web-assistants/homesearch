<?php

namespace App\Mail\User;
 
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels; 
use App\Models\User;

class UserDeleted extends Mailable
{
    use SerializesModels;

    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.user.user_deleted')
                    ->with([
                        'name' => $this->user->name, 
                    ]); 
    }
}
