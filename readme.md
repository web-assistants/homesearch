# Instal #

When installing the system take following steps

- rename .env.example to .env
- update .env file with relevant database settings. NOTE: you have to use MySQL for the database.
- run `composer update` to install component
- run `php artisan migrate`
- run `./vendor/bin/phpunit --testdox`

# Notes #

- The code that needed to be refactured included column 'content' within the select statement, while 'content' column was not defined in any of the tables within the brief. Hence I ignored the column 'content'.

# Files of interest #

### Tests ###
- $tests/Feature/Post/GetPostTest.php
- $tests/Feature/User/DeleteUserTest.php

### Routes ###
- $routes/api.php

### View ###
- $resources/views/email/user/user_deleted.blade.php

### Migrations & Factories ###
- $database/factories/*
- $database/migrations/*

### Providers ###
- $app/Providers/AppServiceProvider.php
- $app/Providers/EventServiceProvider.php
- $app/Providers/RouteServiceProvider.php

### Models ###
- $app/Models/*

### Post Feature ###
I have added an example of me using service and repository design patern. The repository example is relively simple, I could have made it more complex by adding an interface with ability to swap databases 

- $app/Http/Controllers/PostController.php
- $app/Services/PostService.php
- $app/Repositories/PostRepo.php

### User Feature ###
For this feature I didn't include repository. Sometimes, depending on the task at hand, if method is not to be re-used or if project is to run of a single database type (and never to change) it might make sense to keep the database command within the service, which is what I did with `$user->delete()`.

- $app/Http/Controllers/UserController.php
- $app/Services/UserService.php

Event, Listener and Mail

- $app/Events/User/UserDeleted.php
- $app/Listeners/User/SendUserDeletedNotification.php
- $app/Mail/User/UserDeleted.php


