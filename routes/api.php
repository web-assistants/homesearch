<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
  
Route::delete('/user/{user}', ['as' => 'api.user.destroy', 'uses' => 'UserController@destroy']);
Route::get('/post/{post}', ['as' => 'api.post.show', 'uses' => 'PostController@show']);
